#ifndef __xftestfloat_h__

#include <stdint.h>

float square_test_float(float in);
double square_test_double(double in);
float float_add_sub_mul_div(float a, float b, float *sub, float *mul, float *div);
double double_add_sub_mul_div(double a, double b, double *sub, double *mul, double *div);
uint64_t square_test_unsigned(uint32_t in);

#endif
