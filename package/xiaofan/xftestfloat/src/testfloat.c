#include "testfloat.h"

double double_add_sub_mul_div(double a, double b, double *sub, double *mul, double *div) {
    *sub = a - b;
    *mul = a * b;
    *div = a / b;
    return a + b;
}

double square_test_double(double in) {
    return in * in;
}

float float_add_sub_mul_div(float a, float b, float *sub, float *mul, float *div) {
    *sub = a - b;
    *mul = a * b;
    *div = a / b;
    return a + b;
}

float square_test_float(float in) {
    return in * in;
}

uint64_t square_test_unsigned(uint32_t in) {
    return (uint64_t)in * (uint64_t)in; 
}

